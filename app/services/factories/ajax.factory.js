'use strict';

(function(){
	angular
		.module('app')
		.factory('ajaxfactory', ajaxfactory)
		
	ajaxfactory.$inject = ['$http'];

	function ajaxfactory($http) {
		var data = {
			getData : getData
		};

		return data;

		///////////////////

		function  getData(reqObject){
			var dataKeeper;
			return $http({
				url: 'services/' + reqObject.url,
				method: 'POST',
				data: reqObject.data
			})
			.then(success)
			.catch(failure)
			.finally(always);

			///////////////////

			function success(response){
				dataKeeper = response.data;
				wrongPromiseCatcher(dataKeeper);

				///////////////////

				function wrongPromiseCatcher(dataKeeper){
					if(dataKeeper.ReturnType === 'Error'){
						console.log("There is an " + dataKeeper.ReturnType + ":-- " + dataKeeper.ReturnMessage + ' --');
						reqObject.unhappyResponse(dataKeeper);
					} else {
						console.log("Everything looks good, you're awesome!!");
						reqObject.happyResponse(dataKeeper);
					}

				}
			}
			function failure(error){
				dataKeeper = error;
				console.log("Everything looks bad, you're full!!");
				reqObject.unhappyResponse(dataKeeper);
			}

			function always(){
				// Dubbugin Function.. Keep it 'commented' in production
				console.log(dataKeeper);
			}
		}
	}
})();