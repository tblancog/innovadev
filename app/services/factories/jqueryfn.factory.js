'use strict';

(function(){
	angular
		.module('app')
		.factory('jqueryfn', jqueryfn)

	jqueryfn.$inject = [];

	function jqueryfn(jqAnimationPage2){
		var data = {
			setScrollPosition : setScrollPosition,
			perfectScroll : perfectScroll,
			removeModalShadow : removeModalShadow,
			desplegar : desplegar
		}

		return data;

		//////////////////

		function setScrollPosition(){
			if (getScrollPosition.x > 100){

			} else if (getScrollPosition.x > 1000){

			} else if (getScrollPosition.x > 10000){

			};

			//////////////////

			function getScrollPosition(){
				var scrollPosition = { x : 0, y : 0 };
				jQuery(window).scroll(function(){
					scrollPosition.x = jQuery(window).scrollTop();
					scrollPosition.y = jQuery(window).scrollLeft();
				})
				return scrollPosition;
			}
		}

		function perfectScroll(){
			// jQuery('body').perfectScrollbar();
			// jQuery('body').perfectScrollbar({ ... });   // with options
			// jQuery('body').perfectScrollbar('update');  // Update
			// jQuery('body').perfectScrollbar('destroy'); // Destroy
		}

		function removeModalShadow(){
			jQuery('.modal-open').removeClass('modal-open').addClass('modal-close');
			jQuery('.modal-backdrop').remove();
		}

		function desplegar(_this){
			var clicks = (jQuery(_this).css('display') === 'none') ? false : true;
			(clicks) ? 
				jQuery(_this).css({display:'none'}) :
				jQuery(_this).css({display:'block'});
		}
	}
})();