'use strict';

(function(){
	angular
		.module('app')
		.factory('getservices', getservices)

	getservices.$inject = ['ajaxfactory', '$location', 'jqueryfn'];

	function getservices(ajaxfactory, $location, jqueryfn){
		var VM = this;

		var services = {
			getAuthenticated : getAuthenticated,
			getBasicData : getBasicData,
			getInstalledBase : getInstalledBase,
			doPurchase : doPurchase,
			setPrices : setPrices,
			setUser : setUser
		}

		var uservariables = {
			sid : "",
			ptypeid : "7971", // Always Hardocoded
			addid : "",
			snid : "",
			tipoPlan : ""
		}

		var globalPrices = {
			precioHbo : "0",
			precioFox : "0"
		}

		var username = "";
		
		return services;

		///////////////////

		function getAuthenticated(){
			var obj = {
				url: 'isAuth.php',
				happyResponse: function(dataKeeper){
					if (dataKeeper.isInWL){
						jQuery('.close').trigger('click');
						$location.path('selecciona-tu-plan');
						getBasicData();
					} else if(dataKeeper.isLogged){
						alert('No estas autorizado');
						doLogout();
					} else {
						window.location.href = 'https://online.cablevision.com.ar/services/ninja.php';
					}
					return dataKeeper;

					/* else if (ctrlCounter <= 10){
							var timeout = setTimeout(function() { getAuthenticated() }, 2000);
							ctrlCounter++;
					} */
					
				},
				unhappyResponse: function(dataKeeper){
					console.log(dataKeeper.ReturnMessage);
				}
			}

			return ajaxfactory.getData(obj);
		}

		function doLogout(){
			window.location.href = 'services/logoutEdit.php';
			jqueryfn.removeModalShadow();
		}

		function getBasicData(){

			uservariables.tipoPlan = 'full';

			var obj = {
				url: 'getDatosBasicos.php',
				happyResponse: function(dataKeeper){
					uservariables.sid = dataKeeper.usSubscriberId;
					username = dataKeeper.usNombre;
				},
				unhappyResponse: function(dataKeeper){
					console.log(dataKeeper.ReturnMessage);
				}
			}

			return ajaxfactory.getData(obj);
		}

		function getInstalledBase(){
			var obj = {
				url: 'getBaseInstalada.php',
				data: {
					sid: uservariables.sid
				},
				happyResponse: function(dataKeeper){
					uservariables.snid = dataKeeper.contracts[0].SubscriptionId;
					uservariables.addid = dataKeeper.contracts[0].basesList.bases[0].AddressId;
					getCommercialOffer();
					$location.path('selecciona-tu-plan-premium');
				},
				unhappyResponse: function(dataKeeper){
					alert(dataKeeper.ReturnMessage);
				}
			}
			return ajaxfactory.getData(obj);
		}

		function getCommercialOffer(){
			var obj = {
				url: 'getOfertaComercial.php',
				data: {
					ptypeid : uservariables.ptypeid,
					addid : uservariables.addid,
					tipoPlan : uservariables.tipoPlan
				},
				happyResponse: function(dataKeeper){
					var pichi = setTimeout(function(){

						for(var packPre in dataKeeper.premium){
							if( dataKeeper.premium[packPre].ServiceClassId === "32" ) {
								globalPrices.precioHbo = (dataKeeper.premium[packPre].Amount !== "") ? dataKeeper.premium[packPre].Amount : "0";
								if (dataKeeper.premium[packPre].StatusId === "1"){
									jQuery('#hbo').prop( "disabled", true );
								}
							} else if(dataKeeper.premium[packPre].ServiceClassId === "33"){
								globalPrices.precioFox = (dataKeeper.premium[packPre].Amount !== "") ? dataKeeper.premium[packPre].Amount : "0";
								if (dataKeeper.premium[packPre].StatusId === "1"){
									jQuery('#fox').prop( "disabled", true );
								}
							}
						}

					}, 1000);

					jqueryfn.removeModalShadow();
				},
				unhappyResponse: function(dataKeeper){
					alert(dataKeeper.ReturnMessage);
				}
			}
			return ajaxfactory.getData(obj);
		}

		function doPurchase(statusPremium){
			var purchaseOb = {
				tipoPlan : uservariables.tipoPlan,
				statusPremium : statusPremium
			}
			var obj = {
				url: 'comprarInnova.php',
				data: {
					sid : uservariables.sid, 
					snid : uservariables.snid,
					addid : uservariables.addid,
					product : purchaseOb // Producto que carga el cliente, Adicionales, Premium.
				},
				happyResponse: function(dataKeeper){
					// TODO: Función de validación de Positiva..
					$('#ModalFinal').modal();
				},
				unhappyResponse: function(dataKeeper){
					// TODO: Función de validación de Negativa..
					alert("Error: Ya se realizó la compra");
				}
			}
			return ajaxfactory.getData(obj);
		}

		function setPrices(statusPremium){
			var preciosDeRetorno = {
				precioHbo : "0",
				precioFox : "0",
				precioTotal : "0"
			};

			switch(statusPremium){
				case "0":
					preciosDeRetorno.precioHbo = "0";
					preciosDeRetorno.precioFox = "0";
					break;
				case "1":
					preciosDeRetorno.precioHbo = globalPrices.precioHbo;
					preciosDeRetorno.precioFox = "0";
					break;
				case "2":
					preciosDeRetorno.precioHbo = "0";
					preciosDeRetorno.precioFox = globalPrices.precioFox;
					break;
				case "3":
					preciosDeRetorno.precioHbo = globalPrices.precioHbo;
					preciosDeRetorno.precioFox = globalPrices.precioFox;
					break;
			}

			preciosDeRetorno.precioTotal = preciosDeRetorno.precioHbo * 1 + preciosDeRetorno.precioFox * 1;

			return preciosDeRetorno;
		}

		function setUser(){
			return username;
		}
	}
})();