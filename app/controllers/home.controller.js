'use strict';
(function() {
	
	angular
		.module('app')
		.controller('homeCtrl', HomeCtrl);

	HomeCtrl.$inject = ['$anchorScroll', '$location', 'jqueryfn'];

	function HomeCtrl($anchorScroll, $location, jqueryfn){
		var vm = this;

		vm.gotoAnchor = function(x) {
			($location.hash() !== x) ?
				$location.hash(x) :
				$anchorScroll();
		}

		vm.deploy = function(div){
			jqueryfn.desplegar(div);
		}
	}
})();