'use strict';
(function() {
	
	angular
		.module('app')
		.controller('globalCtrl', GlobalCtrl);

	GlobalCtrl.$inject = ['jqueryfn', 'getservices', '$location'];

	function GlobalCtrl(jqueryfn, getservices, $location){
		var vm = this;

		function returnPrevent(){
			history.pushState(null, null, location.href);
			window.onpopstate = function(event) {
				history.go(1);
			};
		}
		returnPrevent();

		vm.redToNinja = function(){
			getservices.getAuthenticated();
		}

		vm.redToNinja = function(){
			if(getservices.getAuthenticated() == 'loggedIn'){
				$location.path('/selecciona-tu-plan');
			} else {
				window.location.href = 'https://online.cablevision.com.ar/services/ninja.php';
			}
		}
	}
})();
