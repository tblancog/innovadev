'use strict';
(function() {
	angular
		.module('app')
		.controller('premiumCtrl', PremiumCtrl)

	PremiumCtrl.$inject = ['getservices', 'jqueryfn', '$location'];

	function PremiumCtrl(getservices, jqueryfn, $location){
		var vm = this;

		function activarBtnModalClose(){
			$location.path('final')
			jqueryfn.removeModalShadow();	
		}

		vm.hbo = "";
		vm.fox = "";
		vm.statusPremium = 0;
		
		vm.priceObj = {
			precioHbo : "0",
			precioFox : "0",
			precioTotal : "0"
		}

		vm.foo = function(){
			vm.statusPremium = (vm.hbo + vm.fox) * 1; // 0- None, 1- HBO, 2-FOX+, 3- Both
			vm.priceObj = getservices.setPrices(vm.statusPremium);
		}

		vm.comprar = function(){
			// $('#ModalFinal').modal();
			activarBtnModalClose();
			getservices.doPurchase(vm.statusPremium);
		}
	}

})();