'use strict';
(function() {
	
	angular
		.module('app')
		.controller('seleccionCtrl', SeleccionCtrl);

	SeleccionCtrl.$inject = ['getservices', 'jqueryfn', '$location'];

	function SeleccionCtrl(getservices, jqueryfn, $location){
		var vm = this;
		vm.usermail = "";
		vm.tipoPlan = "full";
		vm.usuario = "";

		jqueryfn.removeModalShadow();

		vm.usuario = getservices.setUser();

		getservices.getAuthenticated();

		vm.execIB = function(){
			getservices.getInstalledBase();
		}}
})();