'use strict';
(function() {
	
	angular
		.module('app')
		.directive('draggableThing', draggableThing);

	function draggableThing(){
		return function(scope, element, attr) {
			var pureJsElement = element[0];
			pureJsElement.draggable = true;


			element.bind('dragstart', function(event) {
				//do something here.
			});
		}
	}

	function draggable(){
		return function(scope, element) {
        // this gives us the native JS object
        var el = element[0];

        el.draggable = true;

        el.addEventListener('dragstart', function(e) {
			e.dataTransfer.effectAllowed = 'move';
			e.dataTransfer.setData('Text', this.id);
			this.classList.add('drag');
			return false;
        }, false );

        el.addEventListener('dragend', function(e) {
			this.classList.remove('drag');
			return false;
        }, false);
    }
	}
})();