'use strict';
(function() {
	var _head = $('head'),
		_sheets = 
			'<link rel="stylesheet" type="text/css" href="lib/font.awesome/css/font-awesome.min.css">' +
			'<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">' +
			'<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap-theme.min.css">' +
			'<link rel="stylesheet" type="text/css" href="lib/animate/css/animate.css">' +
			'<!-- FONTS -->' +
			'<link rel="stylesheet" type="text/css" href="assets/fonts/Roboto/Roboto.css">' +
			'<!-- /FONTS -->' +
			'<!-- ESTILOS CUSTOM -->' +
			'<link rel="stylesheet" type="text/css" href="assets/css/styles.css">' +
			'<link rel="stylesheet" type="text/css" href="assets/css/cv-custom-fdi.css">';

	var _body = $('body'),
		_scripts = 
			'<!-- MAIN SCRIPTS -->' +
			'<script type="text/javascript" src="lib/angular/js/angular.js"></script>' +
			'<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>' +

			'<!-- Angular Modules -->' +
			'<script type="text/javascript" src="app/app.js"></script>' +
			'<!-- Controllers -->' +
			'<script type="text/javascript" src="app/controllers/user.controller.js"></script>' +
			'<!-- Services -->' +
			'<script type="text/javascript" src="app/services/offer.service.js"></script>' +
			'<!-- Factories -->' +
			'<script type="text/javascript" src="app/services/offer.factory.js"></script>' ;

	_head.append(_sheets);
	_body.append(_scripts);
})();
