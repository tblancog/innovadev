'use strict';
(function() {
	angular
		.module('app', [
			'ngRoute',
			'angular-loading-bar',
			'ngAnimate'
		])
		.config(ProvidersPosition)
		.run(['$anchorScroll', function($anchorScroll) {
			$anchorScroll.yOffset = 0;
		}])

		ProvidersPosition.$inject = ['$routeProvider', '$locationProvider'];

	function ProvidersPosition($routeProvider, $locationProvider){
		$locationProvider.html5Mode(true);
		$routeProvider
			.when('/',{
				controller: 'homeCtrl',
				controllerAs: 'home',
				templateUrl: 'templates/home.html'
			})
			.when('/selecciona-tu-plan',{
				controller: 'seleccionCtrl',
				controllerAs: 'seleccion',
				templateUrl: 'templates/seleccion.html'
			})
			.when('/selecciona-tu-plan-premium',{
				controller: 'premiumCtrl',
				controllerAs: 'premium',
				templateUrl: 'templates/premium.html'
			})
			.when('/final',{
				templateUrl: 'templates/final.html'
			})
			.otherwise({
				redirectTo: '/'
			})
	}
})();