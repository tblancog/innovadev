<?php
session_start();
include('lib/functions.php');

require_once('/var/simplesamlphp/lib/_autoload.php');
$as = new SimpleSAML_Auth_Simple('innova-sp');

$inWL = false;
$loggedIn = false;

if ($as->isAuthenticated()) {
	$data= $as->getAuthData('saml:sp:NameID');
	$data= explode('/',  $data['Value'] );
	$data= trim($data[1]);

	if(checkoutWL('mail', $data) == true ){
		$_SESSION['mail']= $data;
		$inWL = true;
  	} else {
		$inWL = false;
	}
	$loggedIn = true;  
} else {
	$loggedIn = false;
}

header('Content-Type: application/json');
echo json_encode( ['isLogged'=>$loggedIn, 'isInWL'=>$inWL] );
