<?php

interface InnovaInterface{

	public function getContratosCliente($SubscriberID, $SubscriptionID= null);

	public function extractContracts($SubscriberID);

	public function extractBases($list);

	public function extractProducts($productList);

}