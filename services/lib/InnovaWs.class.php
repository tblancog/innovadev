<?php 

class InnovaWs extends SoapClient{

	public $headers;
	protected $wsdl;
	public $options;
	public $params;
	protected $method;
	protected $encoding;

	public function __construct(){
		
		$this->wsdl='';
		$this->options = array(
			'trace'=> 1,
			"exception" => 1
			);
		$this->encoding= 'UTF-8' ;
	}


	public function getParams(){
		
		return $this->params;
	}

	public function setParams($EBMID, $data){
		
		$header= new EBMHeader();
		$header->EBMID= $EBMID;

		$dataArea= new DataArea();

		if(!empty($data)){

			foreach ($data as $key => $d)
				$dataArea->$key = $d;

		}else
			die("ERROR: Empty input");
		
		$this->params = array('EBMHeader'=>$header, 'DataArea'=> $dataArea);

	}

	public function getWsdl(){
		return $this->wsdl;
	}

	public function setWsdl($newWsdl){
		$this->wsdl= $newWsdl;
	}


	public function isSuccessSoapRequest($data){
	
		if($data->ReturnCode == '0')
			return true;
		else{
			die( $data= json_encode(array('ReturnType'=> 'Error',
							'ErrorCode' => $data->ErrorCode,
							'ReturnMessage' => $data->ReturnMessage)));
		}
	}


	public function extractDataArea($data, $dataKey= 'DataArea'){
		return $data[$dataKey];
	}


		public function getError(){

		if( @array_key_exists('return', $this->response) ){

			$status= $this->response['return']['responseStatus']['statusCode'];
			$statusMsg= $this->response['return']['responseStatus']['statusMsg'];
		}else{

			$status= $this->response['ReturnCode'];
			$statusMsg= $this->response['ReturnMessage'];
		}

		if($status == '0')
			$this->error= null;
		else{

			$this->error= ['ReturnType'=> 'Error',
						  'ErrorCode' => $status,
						   'ReturnMessage' => $statusMsg];
		}
	}

	public function object_to_array($obj) {

		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
			$new = array();
			foreach($obj as $key => $val) {
				$new[$key] = $this->object_to_array($val);
			}
		}
		else $new = $obj;
		return $new;
	}
}