<?php

include('InnovaWs.class.php');
include('InnovaInterface.class.php');
include('EBMHeader.class.php');
include('DataArea.class.php');
include('conf/WS.conf.php');
include('conf/Pack.class.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


class InnovaService extends InnovaWs implements InnovaInterface
{
    protected $soap;
    protected $error;
    protected $response;

    public function __construct()
    {

        parent::__construct();

    }

    public function getDomicilio($SubscriberID)
    {

        $this->wsdl = DOMICILIO_WSDL;
        $EBMID = 'domicilio';
        $this->setParams($EBMID, ['SubscriberId' => $SubscriberID, 'SubscriptionId' => null]);
        $client = new SoapClient($this->wsdl, $this->options);
        $data = $client->ConsultarDomicilio($this->params);

        return $data;
    }

    public function getBI($SubscriberID, $SubscriptionID)
    {

        $this->wsdl = BI_WSDL;
        $EBMID = 'baseInstalada';
        $this->setParams($EBMID, ['SubscriberID' => $SubscriberID, 'SubscriptionID' => $SubscriptionID, 'AddressID' => null]);
        $client = new SoapClient($this->wsdl, $this->options);
        $data = $client->ConsultarBaseInstalada($this->params);
        return $data;
    }

    public function getContratosCliente($SubscriberID, $SubscriptionID = null)
    {

        $this->wsdl = CONTRATOS_WSDL;
        $EBMID = 'contratos';
        $this->setParams($EBMID, ['SubscriberID' => $SubscriberID, 'SubscriptionID' => $SubscriptionID, 'AddressID' => null]);
        $client = new SoapClient($this->wsdl, $this->options);
        $data = $client->ConsultarContratosDeCliente($this->params);
        return $data;
    }

    public function extractContracts($SubscriberID)
    {

        $response = $this->getContratosCliente($SubscriberID);

        if ($this->isSuccessSoapRequest($response)) {

            $contracList = @$response->DataArea->ContractList->Contract;
            if ($contracList != NULL) {


                $newContracList = [];

                if (is_array($contracList))
                    $newContracList = $contracList;
                else
                    $newContracList[] = $contracList;

                $newContracList = $this->object_to_array($newContracList);


                $contracts = [];
                foreach ($newContracList as $contract) {

                    $SubscriptionID = $contract['Id'];
                    $billingAddress = $contract['BillingAddress']['LineOne'];
                    $response = $this->getBI($SubscriberID, $SubscriptionID);
                    if ($this->isSuccessSoapRequest($response)) {

                        $basesList = $this->extractBases($response->DataArea->InstalledProductABOList);
                    }

                    $contracts[] = array('SubscriberId' => $SubscriberID, 'SubscriptionId' => $SubscriptionID, 'billingAdress' => str_replace('/', '', $billingAddress), 'basesList' => $basesList);
		   // check out if customer has installed bases
		   if(empty($basesList['bases']))  return ["ReturnType"=>"Error", "ReturnMessage"=> "Este cliente no tiene bases instaladas"] ; 
                    break;
                }

                $resultContracts= ['contracts' => $this->object_to_array($contracts)];
            }
            else{
                try{

                    $resultContracts= ['contracts' => []];
                    throw new Exception("No existen contratos para este cliente", 1003);

                }catch (Exception  $e){
                    return ['ReturnType'=> 'Error',
						    'ErrorCode' => $e->getCode(),
						    'ReturnMessage' => $e->getMessage()];
                }
            }
            return $resultContracts;
        }

    }

    public function extractBases($list)
    {

        $newBasesList = [];
        if (is_array($list->InstalledProductABO))
            $newBasesList = $list->InstalledProductABO;
        else
            $newBasesList[] = $list->InstalledProductABO;

        $bases = [];
        foreach ($newBasesList as $base) {
            $products = $this->extractProducts($base);
            $AddressId = (string)$base->AddressId;

            if ($base->TypeID == "2") {
                $Address = str_replace('/', '', $base->Address);
                $bases[] = array('TypeID' => $base->TypeID, 'TypeDescription' => $base->TypeDescription, 'productList' => $products, 'AddressId' => $AddressId, 'Address' => $Address);

            } elseif ($base->TypeID == "7971") {
                $Address = str_replace('/', '', $base->Address);
                $bases[] = array('TypeID' => $base->TypeID, 'TypeDescription' => $base->TypeDescription, 'productList' => $products, 'AddressId' => $AddressId, 'Address' => $Address);
            }
        }

        return ['bases' => $this->object_to_array($bases)];
    }

    public function extractProducts($productList)
    {


        $newProductList = [];
        if (is_array($productList->InstalledProductList))
            $newProductList = $productList;
        else
            $newProductList[] = $productList;

        $products = [];
        foreach ($newProductList as $product) {
            $type = $product->TypeDescription;
            $classId = property_exists($product->Service, 'ClassId') ? $product->Service->ClassId : '';

            $classDescription = property_exists($product->Service, 'ClassDescription') ? $product->Service->ClassDescription : '';
            $classFamilyDescription = property_exists($product->Service, 'ClassFamilyDescription') ? $product->Service->ClassFamilyDescription : '';

            $desc = $classDescription . " " . $classFamilyDescription;
            $products[] = array('TypeDescription' => $type, 'Service' => [
                'classId' => $classId,
                'ClassDescription' => $type,
                'ClassFamilyDescription' => $desc,
            ]);
        }

        return $products;
    }

public function purchaseInnova($SubscriberId, $SubscriptionId, $AddressId, $compras){
        $pack= new Pack();
        
        // Plan purchasing
        if($compras->tipoPlan== 'full'){
            $purchaseObj= $pack->packFull;
        }

        //  Pack premiums
        switch($compras->statusPremium){
            case 1:
                $purchaseObj[]= $pack->hbo;
                break;
            case 2:
                $purchaseObj[]= $pack->fox;
                break;
            case 3:
                $purchaseObj[]= $pack->hbo;
                $purchaseObj[]= $pack->fox;
                break;
        }

        $compraArray= [];
        foreach ($purchaseObj as $compra)
            $compraArray[]= $compra;
        $client= new SoapClient(COMPRA_INNOVA_WSDL);
        $params['EBMHeader']= ['EBMID'=> 'compra_innova'];
        $params['DataArea']= ['SubscriberId'=> $SubscriberId, 'SubscriptionId'=> $SubscriptionId, 'AddressId'=> $AddressId, 'Compra'=> ['CarritoCompra'=> $compraArray]];

        // Call wsdl function
        $this->response = $client->__soapCall("ComprarAdicionalCarrito", ['parameters'=> $params ]);
        $this->response= $this->object_to_array( $this->response);

        $this->getError();
        if($this->error === null){
            $this->response= $this->extractDataArea($this->response);
            return $this->response;
        }
        else
            return $this->error;
    }

public function getCommercialOffer($AddressId= null, $ProductTypeId= null){

        $client= new SoapClient(WS_OFERTACOMERCIAL);
        $params['EBMHeader']= ['EBMID'=> 'oferta_innova'];
        $params['DataArea']= ['AddressId'=> $AddressId, 'ProductTypeId'=> $ProductTypeId];
        // Call wsdl function
        $this->response = $client->__soapCall("RecibirOfertaComercial", ['parameters'=> $params ]);

        $this->response= $this->object_to_array( $this->response);

        $this->getError();
        if($this->error === null){
            $this->response= $this->extractDataArea($this->response)["CommercialOfferList"]["CommercialOffer"];
            $offers= $this->response;
            $offersArray=[];

            foreach($offers as $offer){
                $offersArray[]= $offer["OfferGroup"];
            }
            return $offersArray;
        }

        else
            return $this->error;
    }

    public function getUserDataFromLogin($mail){
        $client = new SoapClient(WS_DATOSPERSONALES);

        $params['sessionId']= 'innova';
        $params['loginName']= $mail;
        // Call wsdl function
        $this->response = $client->__soapCall("getUserDataFromLogin", ['parameters'=> $params ]);
        $this->response= $this->object_to_array( $this->response);
        $this->getError();
        if($this->error === null) {

            $this->response= $this->extractDataArea($this->response['return'], 'userData');
            $this->response= array_intersect_key($this->response, ['usLogin'=>null, 'usNombre'=>null, 'usSubscriberId'=>null]);
            return $this->response;

        } else
            return $this->error;
    }


}