<?php

//Comprobar CheckList
function checkoutWL($key, $value){

	if (($fs = fopen("lib/whiteList.csv", "r")) !== FALSE) {
		$loop = 0;
		$clientesAptos=[];
    		while (!feof($fs)) {
        		$elementos = fgetcsv($fs,100,";");
		        array_push($clientesAptos, ['SubscriberId'=>$elementos[1],'mail'=>$elementos[0]]);
		        $loop++;
    			}

	}	

	if( count($clientesAptos) > 0) {
		foreach ($clientesAptos as $cte) {
			if($cte[$key] == $value)
				return true;
			}
		}
fclose($fs);
return false;
}

function full($item){
	return ($item["ServiceClassId"]==BASICO || $item["ServiceClassId"]==PREMIUM_HBO || $item["ServiceClassId"]==PREMIUM_FOX || $item["ServiceClassId"]==SERVICIO_AD_DONGLE || $item["ServiceClassId"]==SERVICIO_AD_GRABACION  || $item["ServiceClassId"]==SERVICIO_AD_SIMULTANEIDAD  || $item["ServiceClassId"]==SERVICIO_AD_SVA3 || $item["ServiceClassId"]==SERVICIO_AD_1000);
}
// Crear función callback para agregarle elementos al array de hbo y fox

function light($item){
	return ($item["ServiceClassId"]==BASICO);
//Que tiene el servicio? falta definir
}

function productProcess($array){

	if(count($array) > 0){

		foreach($array as $item){
			if($item['ServiceClassId'] == PREMIUM_HBO || $item['ServiceClassId'] == PREMIUM_FOX){
				$arrayPremium[]=$item;
				$key = array_search($item,$array);
				unset($array[$key]);
			}
		}
		$array = array_values($array);
		$arrayPremium = array_values($arrayPremium);

		$arrayOffer= [];
		$arrayOffer['productos']= $array;
		$arrayOffer['premium']= $arrayPremium;
	}else{

		$arrayOffer= null;
	}

return $arrayOffer;
}

function getAndFilterInt($VALUE)
{
	if ($VALUE!=null)
		{
			return trim(filter_var($VALUE,FILTER_SANITIZE_NUMBER_INT));
		} 
		else 
			return trim($VALUE);
 }


function getAndFilterMail($VALUE)
{
	$email=filter_var($VALUE, FILTER_VALIDATE_EMAIL);
		if (empty($VALUE) && $email==false) return 34;
		if (!empty($VALUE) && $email== false) return 33;
			else return $email;
}


function getAndFilterString($VALUE)
{
$valor =  filter_var($VALUE,FILTER_VALIDATE_STRING);

	if ($valor!=false)
		{
			return $valor;
		} 
		else {
				return null;
			 }
}

function getAndFilterIP($IP){

$valor =  filter_var($IP,FILTER_VALIDATE_IP);	

	if ($valor != false)
		{
			return $valor;
		}
		else {

			return null;
		}
}

function validateToNumeric($value){
	return trim(filter_var($value,FILTER_VALIDATE_INT));
}

function validateToIp($value){
	return (filter_var($value,FILTER_VALIDATE_IP));
}


function errorIDIncorrecto(){
	return array('ReturnType'=> 'Error', 'ErrorCode' => 1000, 'ReturnMessage' => "ID incorrecto");
}

function errorCampoVacio($Campo){
	return array('ReturnType'=> 'Error', 'ErrorCode' => 1001, 'ReturnMessage' => "Campo vacío".$Campo.".");
}

function errorMailIncorrecto(){
	return array('ReturnType'=> 'Error', 'ErrorCode' => 1002, 'ReturnMessage' => "Mail incorrecto");
}

function catchError($e){
	return array('ReturnType'=> 'Error','ErrorCode' => $e->getCode(),'ReturnMessage' => "Error: Error en llamada de servicio web- ".$e->getMessage());
}

function errorIPEmpty(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "Dirección IP vacía");
}

function errorInvalidIP(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "Dirección IP inválida");
}

function errorExecution(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "Error en la llamada del  servicio");
}

function errorCompra(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "No está incluido dentro de nuestros clientes permitidos");
}

function errorAuth(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "Usuario no autenticado");
}
function errorNoBases(){
	return array('ReturnType'=> 'Error', 'ReturnMessage' => "No existen bases instaladas para este cliente");
}
