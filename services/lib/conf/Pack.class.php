<?php

class Pack{

// Plans
    public $packFull = [
        [
            // Innova
            "ProductId" => "newInnova",
            "ProductTypeId" => "7971",
            "CommercialPlanId" => "255",
            "ParentComponentId" => "",
            "ComponentId" => "newCmpInnova",
            "ComponentTypeId" => "7097",
            "ClassServiceId" => "2897",
            "StatusId" => "1",
        ],
        // [
        //     //  Pack 1000
        //     "ProductId" => "newInnova",
        //     "ProductTypeId" => "7971",
        //     "CommercialPlanId" => "255",
        //     "ParentComponentId" => "",
        //     "ComponentId" => "newInnovaPack1000",
        //     "ComponentTypeId" => "7099",
        //     "ServiceClassId" => "2440",
        //     "StatusId" => "1",
        // ],
//        [
//            "ProductId" => "newProduct1",
//            "ProductTypeId" => "7971",
//            "CommercialPlanId" => "255",
//            "ParentComponentId" => "",
//            "ComponentId" => "newCmp9",
//            "ComponentTypeId" => "7099",
//            "ServiceClassId" => "2901",
//            "ServiceClassDescription" => "Pack Simultaneidad",
//            "StatusId" => "1",
//            "Amount" => ""
//        ],
//        [
//            "ProductId" => "newProduct1",
//            "ProductTypeId" => "7971",
//            "CommercialPlanId" => "255",
//            "ParentComponentId" => "",
//            "ComponentId" => "newCmp10",
//            "ComponentTypeId" => "7099",
//            "ServiceClassId" => "2902",
//            "ServiceClassDescription" => "Pack Grabación",
//            "StatusId" => "1",
//            "Amount" => ""
//        ],
        [
            "ProductId" => "newProduct1",
            "ProductTypeId" => "7971",
            "CommercialPlanId" => "255",
            "ParentComponentId" => "",
            "ComponentId" => "newCmp13",
            "ComponentTypeId" => "7099",
            "ServiceClassId" => "2905",
            "ServiceClassDescription" => "Pack Full",
            "StatusId" => "1",
            "Amount" => ""
        ],
//        [
//            "ProductId" => "newProduct1",
//            "ProductTypeId" => "7971",
//            "CommercialPlanId" => "255",
//            "ParentComponentId" => "",
//            "ComponentId" => "newCmp14",
//            "ComponentTypeId" => "7099",
//            "ServiceClassId" => "2906",
//            "ServiceClassDescription" => "Dongle",
//            "StatusId" => "1",
//            "Amount" => ""
//        ]
    ];

    public $hbo = [
        "ProductId" => "newInnova",
        "ProductTypeId" => "7971",
        "CommercialPlanId" => "255",
        "ParentComponentId" => "",
        "ComponentId" => "newCmpInnova",
        "ComponentTypeId" => "7005",
        "ClassServiceId" => "32",
        "StatusId" => "1",
    ];

    public $fox = [
        "ProductId" => "newInnova",
        "ProductTypeId" => "7971",
        "CommercialPlanId" => "255",
        "ParentComponentId" => "",
        "ComponentId" => "newCmpInnova",
        "ComponentTypeId" => "7005",
        "ClassServiceId" => "33",
        "StatusId" => "1",
    ];

    public $both= [
        [
            "ProductId" => "newInnova",
            "ProductTypeId" => "7971",
            "CommercialPlanId" => "255",
            "ParentComponentId" => "",
            "ComponentId" => "newCmpInnova",
            "ComponentTypeId" => "7005",
            "ClassServiceId" => "32",
            "StatusId" => "1",
        ],
        [
            "ProductId" => "newInnova",
            "ProductTypeId" => "7971",
            "CommercialPlanId" => "255",
            "ParentComponentId" => "",
            "ComponentId" => "newCmpInnova",
            "ComponentTypeId" => "7005",
            "ClassServiceId" => "33",
            "StatusId" => "1",
        ],

    ];

}
