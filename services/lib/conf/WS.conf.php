<?php

// Archivo de configuración innova
const CONTRATOS_WSDL= "http://soaosbh.corp.cablevision.com.ar:8011/CablevisionOracleFMW/Autogestion/ABCS/Cliente/V1/ConsultarContratosDeClienteAutogestionReqABCS?WSDL";
const BI_WSDL= 		  "http://soaosbh:8011/CablevisionOracleFMW/Autogestion/ABCS/BaseInstalada/V1/ConsultarBaseInstaladaAutogestionReqABCS?WSDL";
const WS_OFERTACOMERCIAL= "http://soaosbh:8011/CablevisionOracleFMW/Autogestion/ABCS/OfertaComercial/V1/RecibirOfertaComercialAutogestionReqABCS?WSDL";
const COMPRA_INNOVA_WSDL= "http://soaosbh:8011/CablevisionOracleFMW/Autogestion/ABCS/Carrito/V1/ComprarAdicionalCarritoAutogestionReqABCS?WSDL";
const WS_DATOSPERSONALES= "http://jbho1.corp.cablevision.com.ar:8280/IDSAG/UserWS?WSDL";

const TYPEID_CATV =2;
const BASICO =2897;
const PREMIUM_HBO =32;
const PREMIUM_FOX =33;
const SERVICIO_AD_DONGLE =2906;
const SERVICIO_AD_GRABACION =2902;
const SERVICIO_AD_SIMULTANEIDAD =2901;
const SERVICIO_AD_SVA3 =2905;
const SERVICIO_AD_1000= 2440;