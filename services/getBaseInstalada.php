<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header('Content-Type: application/json');

include('lib/InnovaService.class.php');
include('lib/functions.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

try{
	if( !property_exists($request, $request->sid) && empty($request->sid) )
	{
		$response= errorCampoVacio(' SubscriptionId');
	}else
		{
			if (checkoutWL('SubscriberId', $request->sid) == true ) {
   	     		$request->sid= validateToNumeric($request->sid);
				$client = new InnovaService();
				$response = $client->extractContracts($request->sid);
			}
			else $response= errorCompra();
		}

	echo json_encode($response);

}catch(Exception $e){
	$response= catchError($e);
	echo json_encode($response);
}