<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header('Content-Type: application/json');
include('lib/InnovaService.class.php');
include('lib/functions.php');

$request = json_decode(file_get_contents("php://input"));

try{

	$innova= new InnovaService();
	if(
		( !property_exists($request, $request->addid)    && empty($request->addid)  )   ||
		( !property_exists($request, $request->ptypeid)  && empty($request->ptypeid)  ) ||
		( !property_exists($request, $request->tipoPlan) && empty($request->tipoPlan)  ) )
	{
		$response= errorCampoVacio('');
	}else{

		$request->addid= trim($request->addid);
		$request->ptypeid= trim($request->ptypeid);

		$response= $innova->getCommercialOffer( $request->addid, $request->ptypeid);

		if(@$response['ReturnType'] == 'Error' && @$response['ErrorCode'] == 'SMFLX-13'){

			$response= errorExecution();
		}elseif( @$response['ReturnType'] == 'Error'){

			$response= errorIDIncorrecto();
		}else{

			$arrayProductos = array_filter($response, $request->tipoPlan);
			$response= ProductProcess($arrayProductos);
		}
	}	
	echo json_encode($response);

}catch(Exception $e){
	$response= catchError($e);
	echo json_encode($response);
}
