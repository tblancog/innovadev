<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header('Content-Type: application/json');
include('lib/InnovaService.class.php');
include('lib/functions.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$sid = getAndFilterInt($request->sid);
$snid = getAndFilterInt($request->snid);
$addid = getAndFilterInt($request->addid);

try{

	$innova= new InnovaService();
	$response= $innova->purchaseInnova($sid,
		                               $snid,
		                               $addid,
		                               $request->product);
	echo json_encode($response);

}catch(Exception $e){
	$response= catchError($e);
	echo json_encode($response);
}
