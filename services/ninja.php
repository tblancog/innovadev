<?php
session_start();
$r=require_once('/var/simplesamlphp/lib/_autoload.php');
$as = new SimpleSAML_Auth_Simple('innova-sp');

$as->requireAuth(array(
    'ReturnTo' => 'https://online.cablevision.com.ar/selecciona-tu-plan',
    'KeepPost' => FALSE,));

$data= $as->getAuthData('saml:sp:NameID');

$data= explode('/',  $data['Value'] );
$data= trim($data[1]);
$_SESSION['mail']= $data;
